/*============================================
DISCUSSION

MODULE: WD004-S5-MULTI-DIMENSIONAL ARRAYS & OBJECTS

MULTI-DIMENSIONAL ARRAYS
- Recall that array is an ___index__ collection of data.
- Multi-dimensional arrays are ___nested___ arrays or arrays that 
contain another array as an element.
===============================================*/

let b44 = [
	'5:30 - 9:30 PM',
	'Joyce Perez',
	28,
	[ 
		"student 1",
		"student 2",
		"student 3",
		"student 4",
		"student 5"
	],
	[
		"activity 1",
		"activity 2",
		"activity 3",
		[
			"dicussion",
			"code along",
			"reinforcement",
			"capstone"
		]
	]

];

// LENGTH
console.log("**************// LENGTH*****************");
console.log(`There are ${b44.length} elements in b44 array.`);
console.log(`There are ${b44[3].length} elements in the nested array.`);
console.log(`There are ${b44[2].length} elements in the nested array.`);

// DISPLAY ELEMENTS ONE BY ONE
console.log("**************DISPLAY ELEMENTS ONE BY ONE*****************");
console.log(`The first element is ${b44[0]}.`);
console.log(`The second element is ${b44[1]}.`);
console.log(`The third element is ${b44[2]}.`);
console.log(`The fourth element is ${b44[3]}.`);



// DISPLAY ELEMENTS OF NESTED ARRAY ONE BY 0NE
console.log("**************DISPLAY ELEMENTS OF NESTED ARRAY ONE BY 0NE*****************");
console.log(`The first element of the nested array is ${b44[3][0]}.`);
console.log(`The second element of the nested array is ${b44[3][1]}.`);
console.log(`The third element of the nested array is ${b44[3][2]}.`);
console.log(`The fourth element of the nested array is ${b44[3][3]}.`);
console.log(`The fifth element of the nested array is ${b44[3][4]}.`);


// DISPLAY ELEMENTS THROUGH A LOOP
console.log("**************DISPLAY ELEMENTS THROUGH A LOOP*****************");

console.log("*****LOOP 1****");
for(let i=0; i<=b44.length-1; i++) {
	//console.log(`Element #${i+1} is ${b44[i]}.`);
	console.log(b44[i]);
	let element = b44[i];
	// if(typeof element === 'object') {
	// 	for(let x=0; x<=b44[i].length-1; x++) {
	// 		console.log(b44[i][x]);
	// 	}
	// }

	// if array has many nested elements and you only want to display b44[3]
	if(typeof element === 'object') {
		for(let x=0; x<=element.length-1; x++) {
			console.log(element[x]);
		}
	}
}


console.log("*****LOOP 2****");
for(let i=0; i<=b44.length-1; i++) {
	//console.log(`Element #${i+1} is ${b44[i]}.`);
	console.log(b44[i]);

	let element = b44[i];
	if(typeof element === 'object') {
		console.log("*****NESTED LOOP 1****");
		for(let x=0; x<=element.length-1; x++) {
			console.log(element[x]);

			let element2 = element[x];		//b44[i][x]
			if (typeof element2 === 'object') {
				console.log("*****NESTED LOOP within a nested loop****");
				for(let y = 0; y<=element2.length-1; y++){
					console.log(element2[y]);	//b44[i][x][y]
				}
			}
		}
	}
}



