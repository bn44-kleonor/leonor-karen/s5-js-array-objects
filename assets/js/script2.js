/*=======================================================
REINFORCEMENT

MODULE: WD004-S4-ARRAY MANIPULATION (ACTIVITY)

1. Create a students array with the strings: adam, emma, and isla
2. Display/Read the contents of the students array.
3. Add a student in the student array.
4. Delete a student in the students array.
5. Update a student in the students array.
*/

/*1. Create a students array with the strings: adam, emma, and isla*/

let students = ["adam", "emma", "isla"];

/*2. Display/Read the contents of the students array.*/
function displayStudents() {
	for(count = 0; count <= students.length-1; count++) {
		console.log(`Student ${count+1}: ${students[count]}`);
	}
}

displayStudents();

/*3. Add a student in the student array.*/
let newStudent = prompt("New student name is: ");
function addStudent(newStudent){
	if(newStudent){
		console.log("You added: " + newStudent);
		students.push(newStudent);
		displayStudents();
	}
}

addStudent(newStudent);

/*4. Delete a student in the students array.*/

let studentToDelete = prompt("What do you want to delete?");

function deleteStudent(studentToDelete) {
	let studentIndex = students.indexOf(studentToDelete);
	if(studentToDelete && studentIndex !== -1) {
		console.log(`You removed ${studentToDelete}`);
		students.splice(studentIndex, 1);
		displayStudents();
	}
}

deleteStudent(studentToDelete);


/*5. Update a student in the students array.*/
let studentToUpdate = prompt("What do you want to update?");
function updateStudent(studentToUpdate) {
	let studentIndex = students.indexOf(studentToUpdate);
	if(studentToUpdate && studentIndex !== -1) {
		let updatedStudent = prompt(`Update ${studentToUpdate} to: `);
		if(updatedStudent) {
			students.splice(studentIndex, 1, updatedStudent);
			console.log(`You have changed ${studentToUpdate} to ${updatedStudent}`);
		}
	}
	displayStudents();
}

updateStudent(studentToUpdate);

