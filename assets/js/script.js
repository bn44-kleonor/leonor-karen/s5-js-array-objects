/* ========================================================
MODULE: WD004-S4-ARRAY MANIPULATION (ACTIVITY)

1. Initialize a "tasks" array with the strings: eat,sleep, & code stored in it. */
let tasks = ["eat", "sleep", "code"];


/*2. Declare a variable "count".*/
let count;

/*3. Initialize a "displayTasks" function that CONTAINS the ff functionalities:
  a) it should iterate all the elements of the tasks array
  b) it should output the following:
      Task 1: eat
      Task 2: sleep
      Task 3: code*/

function displayTasks() {
	for(count = 0; count <= tasks.length-1; count++) {
		console.log(`Task ${count+1}: ${tasks[count]}`);
	}
}

displayTasks();

/*4. Initialize an "addTask" function that accepts "newTask" as its parameter and CONTAINS 
   the ff functionalites:
  a)  it should add the value of newTask at the end of the array
  b)  it should output the strings that you will add in step 5 
      so that your console will now display the following:
      Task 1: eat
      Task 2: sleep
      Task 3: code
      You added: go home
      Task 1: eat
      Task 2: sleep
      Task 3: code
      Task 4: go home
      You added: enjoy the weekend
  c)  it should then call back the function that DISPLAYS the updated list of tasks. 
      Yes, you can call a different function WITHIN a function.*/

let newTask = prompt("What task do you want to add?");
function addTask(newTask){
	if(newTask){
		console.log("You added: " + newTask);
		tasks.push(newTask);
		displayTasks();
	}
}

// addTask("go Home");
// addTask("enjoy the weekend");
addTask(newTask);


/*5. pass the following as arguments to addTask function:
  - go home
  - enjoy the weekend*/



/*6. Initialize a "deleteTask" function that CONTAINS the ff functionalities:
  a) it should output in the console the string you removed (i.e., the task you removed) 
     so that your console will now display the following:
    Task 1: eat
    Task 2: sleep
    Task 3: code
    You added: go home
    Task 1: eat
    Task 2: sleep
    Task 3: code
    Task 4: go home
    You added: enjoy the weekend
    Task 1: eat
    Task 2: sleep
    Task 3: code
    Task 4: go home
    Task 5: enjoy the weekend
    You removed: enjoy the weekend
    Task 1: eat
    Task 2: sleep
    Task 3: code
    Task 4: go home
  b) it should delete the the LAST TASK in the "tasks" array
  c) it should then call back a function that DISPLAYS the updates list of tasks*/

function deleteTask() {
	console.log(`You removed: ${tasks[tasks.length-1]}`);
	tasks.pop;
	displayTasks;
}

deleteTask(); 

/*SAVE AS: s4-a1-array-manipulation*/

/*STRECH GOAL:
deleteTask function should delete the item that you wish to delete. 
Check your notes. We covered this! :-)  */

let taskToDelete = prompt("What do you want to delete?");

function deleteTask(task) {			// (task) because it can be any variable name when inside the function being initialized
	let taskIndex = tasks.indexOf(task);
	if(task && taskIndex !== -1) {
		console.log(`You removed ${task}`);
		tasks.splice(taskIndex, 1);
		displayTasks();
	}
}

deleteTask(taskToDelete);

/*===========================================================*/


/*======== How to Update contents of array =========*/
/* Use splice to update a task */

/** my ans **/
let updateTheTask = prompt("What do you want to update?");
let changeWithTask = prompt("What new value do you want to add?");
function updateTask(updateTheTask, changeWithTask) {			// (task) because it can be any variable name when inside the function being initialized
	let taskIndex = tasks.indexOf(updateTheTask);
	if(updateTheTask && changeWithTask && taskIndex !== -1) {		
			tasks.splice(taskIndex, 1, changeWithTask);	
	}
	displayTasks();
}

updateTask(updateTheTask, changeWithTask);



/* Ms. Joyce ans*/
let taskToUpdate = prompt("What do you want to update?");
function updateTask(taskToUpdate) {
	let taskIndex = tasks.indexOf(taskToUpdate);
	if(taskToUpdate && taskIndex !== -1) {
		let updatedTask = prompt(`Update ${taskToUpdate} to: `);
		if(updatedTask) {
			console.log(taskToUpdate);
			tasks.splice(taskIndex, 1, updatedTask);
			console.log(`You have changed ${taskToUpdate} to ${updatedTask}`);
		}
	}
	displayTasks();
}

updateTask(taskToUpdate);

