/*============================================
DISCUSSION

MODULE: WD004-S5-MULTI-DIMENSIONAL ARRAYS & OBJECTS

JS OBJECTS
- If array is an indexed collection of data, objects are __KEYED__ collection 
of data (aka properties).
- We access values through their respective KEYS.

===============================================*/

//CREATING AN OBJECT
console.log('***********CREATING AN OBJECT************');
let myObject = {};

let profileArr = ["John", "Emma", "Sofia", "Theresa"];
let profile = {
	firstName: "John",		//key(properties) --> firstName		valueofkey --> John
	lastName: "Doe"
};

console.log(profileArr);
console.log(profile);


//ACCESSING PROPERTY OF AN OBJECT
console.log('**********ACCESSING PROPERTY OF AN OBJECT*************');
console.log(profile[0]);		//undefined
console.log(profile["firstName"]);	//via bracket notation
console.log(profile.lastName);		//via dot notation
console.log(`My name is ${profile.lastName}, ${profile.firstName} ${profile.lastName}.`);


//ADDING/CREATING NEW PROPERTIES OF AN OBJECT
console.log('***********ADDING/CREATING NEW PROPERTIES OF AN OBJECT************');
profile['age'] = 12;		//bracket notation
profile.isWorking = false;	//dot notation
profile.isMarried;			// will not be reflected to profile
console.log(profile);
console.log(profile.isMarried);	//undefined



//CHANGING VALUES OF PROPERTIES OF AN OBJECT
console.log('***********CHANGING VALUES OF PROPERTIES OF AN OBJECT************');
profile.firstName = 'Juan';
profile['lastName'] = 'dela Cruz';
console.log(profile);


//NESTED OBJECTS
console.log('***********NESTED OBJECTS************');
let student = {
	name: {
		first: "Emma",
		last: "Watson"
	},
	age: 22,
	address: {
		temporary: {
			unit: 44,
			bldg: 'Hermosa Tower',
			brgy: 'Bangkal',
			city: 'Makati City',
			province: 'Metro Manila'
		},
		permanent: {
			unit: 1,
			bldg: null,
			brgy: 'San Antonio',
			city: 'General Santos City',
			province: 'South Cotabato'

		}
	}
}

console.log(student);
console.log(student.name.first);
console.log(student.name.last);
console.log(student.age);
console.log(student.address.temporary.unit);
console.log(student.address.temporary.bldg);
console.log(student.address.temporary.brgy);
console.log(student.address.temporary.city);
console.log(student.address.temporary.province);
console.log(student.address.permanent.unit);
console.log(student.address.permanent.bldg);
console.log(student.address.permanent.brgy);
console.log(student.address.permanent.city);
console.log(student.address.permanent.province);
console.log(student.address.permanent.isOwner);	//what if a key not determined (e.g. isOwner) --> result is 'undefined'


//ACCESSING PROPERTY OF AN NESTED OBJECTS
//CHANGING PROPERTY OF NESTED OBJECTS
console.log('**********ACCESSING PROPERTY OF AN NESTED OBJECTS*************');
student.address.permanent.bldg = 'N/A';
console.log(student.address.permanent.bldg);


//ADDING PROPERTY TO A NESTED OBJECT
console.log('**********ADDING PROPERTY TO A NESTED OBJECT*************');
student.address.permanent.isOwner = true;
console.log(student.address.permanent);


//FUNCTIONS AND OBJECTS
console.log('************FUNCTIONS AND OBJECTS***********');
let person = {
	first: "Rodrigo",
	last: "Duterter",
	age: 72,
	func: function() {				//property ni object so ang tawag sa kanya is 'method'
		return `Pres. ${this.first} ${this.last} is ${this.age} years old.`;	//this
	}
}

console.log(person.func());


//ARRAY AND OBJECTS
console.log('***********ARRAY AND OBJECTS************');
let studentProfiles = [];
studentProfiles.push(student);		//ipasok ang 'student' object sa array
console.log(studentProfiles[0]);
console.log(studentProfiles[1]);



//DISPLAY ARRAY OF OBJECTS
console.log('***********DISPLAY ARRAY OF OBJECTS************');
function displayStudents(){
	for(let i=0; i<=studentProfiles.length-1; i++){
		console.log(studentProfiles[i].name.first);
		console.log(studentProfiles[i].name.last);
		console.log(studentProfiles[i].name.age);
		console.log(studentProfiles[i].name.address.temporary);
		console.log(studentProfiles[i].name.address.permanent);
	}
}

displayStudents();