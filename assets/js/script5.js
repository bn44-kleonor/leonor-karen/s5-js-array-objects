//1 Create an array of five movie objects.
//Each movie should have a title, rating, and hasWatched properties.
//Loop through the array and print it out via console

//folder: s5-js-array-objects


let movie = [
				{
				title: "Bible project",
				rating: 1,
				hasWatched: true
				},

				{
				title: "Avengers",
				rating: 2,
				hasWatched: true
				},

				{
				title: "Spiderman",
				rating: 3,
				hasWatched: true
				},

				{
				title: "Justice League",
				rating: 4,
				hasWatched: true
				},

				{
				title: "Toy Story",
				rating: 5,
				hasWatched: false
				}
];


function displayMovie(){
	for(let i=0; i<=movie.length-1; i++){
		console.log("Title: " + movie[i].title);
		console.log("Rating: " + movie[i].rating);
		console.log("hasWatched: " + movie[i].hasWatched);
	}
}

displayMovie();


